from findEpiMag import *
from sys import argv, exit
from numpy import mean, log
import pickle
import time

class Mdp:
    def __init__(self, lat, lon, intensities):
        self.lat = [float(i) for i in lat]
        self.lon = [float(i) for i in lon]
        self.intensities = [float(i) for i in intensities]

class Epicenter:
    def __init__(self, lat, lon, mdps, depth):
        self.lat = lat
        self.lon = lon
        self.mdps = mdps
        self.depth = depth

def f1(m,r, a, b, c, d):
    return a * m - b * log(r) - c * r + d

def f2(m,r, a, b, c):
    return a * m - b * log(r) + c

def f3(m,r, a, b, c):
    return a + b * m - c * r

try:
    inputf1 = open(argv[1], 'r')
    inputf2 = open(argv[2], 'r')
    inputf3 = open(argv[3], 'r')
except IOError:
    print("Files not found")
    exit()
except IndexError:
    print("Need to pass the catalog, mdp and parameter files.")
    exit()

param = pickle.load(inputf3)
intensity = []
lats = []
lons = []
n=1
for evt in inputf1.readlines():
    start = time.time()
    l1 = list(evt.split(','))
    evt_id = l1[15]
    h = float(l1[35])
    for mdp in inputf2.readlines():
        l2 = list(mdp.split(','))
        mdp_id = l2[0]
        if mdp_id == evt_id:
            intensity.append(l2[10])
            lats.append(float(l2[7]))
            lons.append(float(l2[8]))
    mdp = Mdp(lats, lons, intensity)
    ep = Epicenter(mean(lats), mean(lons), mdp, h)
    findEpiMag(ep, param, f1)
    intensity = []
    lats = []
    lons = []
    inputf2.seek(0)
    end = time.time()
    print("Evento {} Tempo:{}".format(n, end - start))
    n = n + 1

