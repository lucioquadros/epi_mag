from numpy import max, min, arange, sqrt, mean
from haversine import haversine

def find_r(loc1, loc2, h):
    d = haversine(loc1, loc2)
    return sqrt(d**2 + h**2)

def findEpiMag(epicenter, param, func):
    res = []
    depth = epicenter.depth
    intensities = epicenter.mdps.intensities
    lats = epicenter.mdps.lat
    lons = epicenter.mdps.lon

    leastrms = 100

    lat1 = min(lats)
    lat2 = max(lats)
    lon1 = min(lons)
    lon2 = max(lons)

    for mag in arange(1, 10, 0.1):
        for eplat in arange(lat1, lat2, 0.1):
            for eplon in arange(lon1, lon2, 0.1):
                for mdp in zip(lats, lons, intensities):
                    mdplat, mdplon, i_ob = mdp
                    if i_ob == 0 or i_ob == -1:
                        continue
                    r = find_r((mdplat, mdplon), (eplat, eplon), depth)
                    i_calc = func(mag, r, *param)
                    res.append((i_calc - i_ob)**2)
                rms = sqrt(mean(res))
                if rms < leastrms:
                    foundlat = eplat
                    foundlon = eplon
                    foundmag = mag
                    leastrms = rms
                res = []
    print('lat = {} lon = {} mag = {} rms = {}'.format(foundlat, foundlon, foundmag, leastrms))